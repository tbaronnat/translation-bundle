#Usage
First, use twig function in your main template to append all translations for specific domain : 

````
{{ js_translations(app.request.getLocale(), 'messages')|raw }}
````

Then, to translate any key from this domain, use javascript function :

In messages.en.yml

````
message.hello: "Hello %name%"
````

Then, to translate with javascript:

````
const params = {"%name%": "Theo"};

let translation = TRANSLATIONS.trans('message.hello', params)

translation = "Hello Theo"
