<?php

namespace TBaronnat\TranslationBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use TBaronnat\TranslationBundle\DependencyInjection\TranslationExtension;

class TBaronnatTranslationBundle extends Bundle
{
    public function getContainerExtension()
    {
        if ($this->extension == null) {
            $this->extension = new TranslationExtension();
        }

        return $this->extension;
    }
}