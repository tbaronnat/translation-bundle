(function () {
    'use strict';

    const TRANSLATIONS = {
        trans: (translationKey, params) => {
            let translation = document.querySelector('[data-translation_key="'+translationKey+'"]').data('translation_value');
            if (params !== null && params != 'undefined') {
                for (let key in params) {
                    translation = translation.replace('{'+key+'}', params[key]);
                }
            }
            return translation;
        },
    };

    window.TRANSLATIONS = TRANSLATIONS || {};

})();