<?php

namespace TBaronnat\TranslationBundle\Extension;

use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TranslationsExtension extends AbstractExtension
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getFunctions()
    {
        return array_merge(parent::getFunctions(), [
            new TwigFunction('js_translations', [$this, 'generateJsTranslations'], ['needs_environment' => true]),
        ]);
    }

    public function generateJsTranslations(
        Environment $twig,
        string $locale = 'fr',
        string $translationDomain = 'messages'
    ): string {
        $translations = $this->translator->getCatalogue($locale)->all()[$translationDomain] ?? [];

        try {
            return $twig->render('@TBaronnatTranslation/translations.html.twig', [
                'translations' => $translations
            ]);
        } catch (\Throwable $e) {
        }
        return '';
    }
}
